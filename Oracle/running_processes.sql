SELECT sess.process, sess.status, sess.username, sess.schemaname, sql.sql_text
  FROM v$session sess,
       v$sql     sql
 WHERE sql.sql_id(+) = sess.sql_id
   AND sess.type     = 'USER';

SELECT a.USERNAME,
       d.SPID 进程号,
      -- b.hash_value , d.addr  ,
       to_char(cast((c.sofar / totalwork * 100) as decimal(18, 1))) || '%'  执行百分比,
       cast(c.time_remaining / 60 as decimal(18, 2))  预计剩余_分,
       b.sql_text SQL内容
  from v$session a, v$sqlarea b, v$session_longops c, v$process d
 where a.sql_hash_value = b.HASH_VALUE
   and a.sid = c.sid(+)
   and a.SERIAL# = c.SERIAL#(+)
   and a.PADDR = d.ADDR;
   
