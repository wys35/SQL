MERGE INTO bonuses B
   USING (SELECT employee_id, salary, department_id FROM employees
   WHERE department_id = 80) Sal
   ON (B.employee_id = Sal.employee_id)
   WHEN MATCHED THEN UPDATE SET B.bonus = B.bonus + Sal.salary*.01
     DELETE WHERE (Sal.salary > 50000)
   WHEN NOT MATCHED THEN INSERT (B.employee_id, B.bonus)
     VALUES (Sal.employee_id, Sal.salary*.01)
     WHERE (Sal.salary <= 50000);
