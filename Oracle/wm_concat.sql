select u.id 账户ID,
       u.user_code 名称,
       u.create_time 创建时间,
       u.terminal_id 终端号码,
       wm_concat(sr.name) 角色,
       u.user_name 姓名,
       decode(u.flag,
              '0',
              '超级管理员',
              '1',
              '省级运营商',
              '2',
              '地市运营商',
              '3',
              '代理商 ',
              '4',
              '商户',
              '5',
              'SHOPPINGMALL用户') 用户标识,
       u.valid_time 账户有效期,
       wm_concat(ra.area_code) 数据权限,
       un.name 单位
  from t_sys_user_v2       u,
       t_sys_user_role_v2  r,
       t_sys_unit_v2       un,
       t_sys_role_v2       sr,
       t_sys_user_range_v2 ra
 where u.id = r.user_id
   and un.id = u.unit_id
   and sr.id = r.role_id
   and ra.user_id = u.id
   and u.flag!=4
 group by u.id,
          u.user_code,
          u.create_time,
          u.terminal_id,
          u.user_name,
          u.flag,
          u.valid_time,
          un.name