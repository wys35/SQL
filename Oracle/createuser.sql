create user marketprofiler
identified by marketprofiler
default tablespace market;

GRANT CONNECT TO marketprofiler;
GRANT create session TO marketprofiler;
GRANT create table TO marketprofiler;
GRANT UNLIMITED TABLESPACE TO marketprofiler;
