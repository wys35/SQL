select pricedate, to_char(pricedate, 'yyyy'), goldprice,
avg(goldprice) over (partition by to_char(pricedate, 'yyyy'))
from commodityprice
