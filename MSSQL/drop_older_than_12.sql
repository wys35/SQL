DECLARE @sql NVARCHAR(MAX) = N'';

;WITH p(o,d) AS
(
    SELECT QUOTENAME(SCHEMA_NAME([schema_id])) + '.' + QUOTENAME(name),
        d = RIGHT(REPLACE(name, '_', ''), 6) + '01'
      FROM sys.tables 
      WHERE ISDATE(RIGHT(REPLACE(name, '_', ''), 6) + '01') = 1 
)
SELECT @sql += 'DROP TABLE ' + o + ';' FROM p
    WHERE d < CONVERT(CHAR(8), DATEADD(MONTH, -12, CURRENT_TIMESTAMP), 112);

PRINT @sql;