/*
存储过程名称：PageCut
新增人员：顾加春
新增日期：年月日
描述：分页的统一方法
*/
create PROCEDURE [dbo].[PageCut]
@sqlSelect nvarchar(4000), --from 以后的sql语句 不包含from  不包含Where 不包含Order by
@PrimaryKey nvarchar(100), ---第一个主键不可以为空
@PrimaryKey1 nvarchar(100)='',   ---第二个主键可以为空
@Fields nvarchar(4000),  ---要取得的字段
@strWhere nvarchar(4000)='',----where条件不包含where
@OrderBy nvarchar(4000),---排序顺序 不包含order by
@PageSize int, -- 每页数据行数
@toPage int, --要转到的页码第一页为
@Dist as tinyint = 0 ----是否添加查询字段的DISTINCT 默认不添加/1添加
AS
BEGIN
	declare @strSql  varchar(8000)
	declare @pKey varchar(4000)
	declare @curPage int
	declare @pos int
	declare @strdist as nvarchar(20)
	
	----得到总数
	if @Dist=0
		set @strdist=' '
	else
		set @strdist=' DISTINCT '
	
	set @strSql = 'select  count('+@strdist+'' + @PrimaryKey + ') as Counts from ' + @sqlSelect
	
	

	if @strWhere<>''
		set @strWhere = ' where ' + @strWhere
	else
		set @strWhere =' where 1=1 '

	set @strSql = @strSql + @strWhere
	
	--print (@strSql)
	exec(@strSql)

	set @curPage = @toPage
	
	if @PrimaryKey1<>''
		set @pKey = '(str(isnull('+@PrimaryKey + ',0))+str(isnull(' + @PrimaryKey1+',0)))'
	else
		set @pKey = @PrimaryKey
	
	set @strSql = ' select '+@strdist+' top ' + str(@PageSize) + ' '+@fields + ' from ' + @sqlSelect + @strWhere 
	

	

	if @curPage <> 0
		begin
			set @strSql = @strSql +  ' and ' + @pKey + ' not in '
		end
	if @OrderBy<>''
		begin
			  set @pos=CHARINDEX(@PrimaryKey,@OrderBy)
			  if @pos <=0
				begin
					set @OrderBy = @OrderBy + ',' + @PrimaryKey + ' asc'
				end
		end
	else
		set @OrderBy = @PrimaryKey +' asc'

	
	
	if @curPage <> 0
		begin
			if @OrderBy<>''
				set @strSql = @strSql + '(select '+@strdist+' top ' + str(@curPage*@PageSize) + ' ' +@pKey + ' from ' + @sqlSelect + @strWhere + ' order by ' + @OrderBy + ' ) order by ' +  @OrderBy
			else
				set @strSql = @strSql + '(select '+@strdist+' top ' + str(@curPage*@PageSize) + ' ' +@pKey + ' from ' + @sqlSelect + @strWhere + ')'
		end
	else
		begin
			
			if @OrderBy<>''
				begin
					
					set @strSql = @strSql + ' order by ' +  @OrderBy
					
				end
		end

	--print (@strSql)


	exec(@strSql)
	
END

