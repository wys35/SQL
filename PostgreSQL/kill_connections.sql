/* Script to kill all running connections by specifying a database name: */
	
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE datname = 'datbase_name'
  AND pid <> pg_backend_pid();

/* Script to kill all running connections of a current database: */
	
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE datname = current_database()
  AND pid <> pg_backend_pid();