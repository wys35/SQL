-- https://www.dbrnd.com/2015/11/postgresql-execute-vacuum-full-without-disk-space/
-- Check disk space usage
SELECT pg_size_pretty(pg_database_size('AxiomSQL'));

select table_schema, table_name, pg_size_pretty(pg_total_relation_size(quote_ident(table_name)) ) from information_schema.tables where table_type='BASE TABLE' and table_schema = 'public' order by pg_total_relation_size(quote_ident(table_name)) desc

-- STEP 1 Create temp tablespace
create tablespace temptablespace location 'D:\temp';

-- Verify default table space, null indicates pg_default
SELECT tablespace FROM pg_tables WHERE tablename in (select table_name from information_schema.tables where table_type='BASE TABLE' and table_schema = 'public')

-- Construct alter vaccuum alter statements table by table
select 'alter table ' || table_name || ' set tablespace temptablespace;' || E'\n' || 'VACUUM FULL ' || table_name || ';' || E'\n' || 'ALTER TABLE ' || table_name || ' SET TABLESPACE pg_default;' from information_schema.tables where table_type='BASE TABLE' and table_schema = 'public' order by pg_total_relation_size(quote_ident(table_name)) desc


--STEP 2 copy, remove quotes, execute line by line
"alter table report_global_factor_perfatt_factor_detail set tablespace temptablespace;
VACUUM FULL report_global_factor_perfatt_factor_detail;
ALTER TABLE report_global_factor_perfatt_factor_detail SET TABLESPACE pg_default;"

# STEP 3 Drop the temp tablespace
DROP TABLESPACE temptablespace;